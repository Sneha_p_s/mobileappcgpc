// ignore_for_file: prefer_typing_uninitialized_variables, avoid_print

import 'dart:developer';

import 'package:cgpc_app/dbHelpper/constant.dart';
import 'package:cgpc_app/studentDBModel.dart';
import 'package:mongo_dart/mongo_dart.dart';
// import 'package:first/MongoDBModel.dart';
// import 'package:first/dbHelpper/constant.dart';
// import 'package:mongo_dart/mongo_dart.dart';

class MongoDatabase {
  static var db, loginCollection, studentCollection;
  static connect() async {
    db = await Db.create(MONGO_CONN_URL);
    await db.open();
    inspect(db);
    loginCollection = db.collection(LOGIN_COLLECTION);
    studentCollection = db.collection(STUDENT_COLLECTION);
  }

  static Future<String> insert(StudentDbModel data) async {
    try {
      var result = await studentCollection.insertOne(data.toJson());
      if (result.isSuccess) {
        return "Data Inserted ";
      } else {
        return "Something wrong while inserting data";
      }
    } catch (e) {
      print(e.toString());
      return e.toString();
    }
  }
}
