// ignore: duplicate_ignore
// ignore: file_names
// // To parse required this JSON data, do
// //
// //     final student = studentFromJson(jsonString);

// import 'dart:convert';

// List<Student> studentFromJson(String str) => List<Student>.from(json.decode(str).map((x) => Student.fromJson(x)));

// String studentToJson(List<Student> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Student {
//     Student({
//         required this.id,
//         required this.ktuId,
//         required this.firstName,
//         required this.secondName,
//         required this.email,
//         required this.gender,
//         required this.dob,
//         required this.contact,
//         required this.citizenship,
//         required this.guardianName,
//         required this.guardianMobile,
//         required this.languages,
//         required this.category,
//         required this.differentlyAbled,
//         required this.disabilityType,
//         required this.disabilityPer,
//         required this.academic,
//         required this.studentStatus,
//         required this.studentPhoto,
//         required this.bio,
//         required this.skills,
//         required this.the61B5D8B85A0D0000C0005D05,
//         required this.curYop,
//     });

//     ObjectId id;
//     String ktuId;
//     String firstName;
//     String secondName;
//     String email;
//     String gender;
//     Dob dob;
//     Contact contact;
//     String citizenship;
//     String guardianName;
//     String guardianMobile;
//     String languages;
//     String category;
//     String differentlyAbled;
//     String disabilityType;
//     DisabilityPer disabilityPer;
//     Academic academic;
//     String studentStatus;
//     String studentPhoto;
//     String bio;
//     String skills;
//     String the61B5D8B85A0D0000C0005D05;
//     String curYop;

//     factory Student.fromJson(Map<String, dynamic> json) => Student(
//         id: Id.fromJson(json["_id"]),
//         ktuId: json["ktu_id"],
//         firstName: json["first_name"],
//         secondName: json["second_name"],
//         email: json["email"],
//         gender: json["gender"],
//         dob: Dob.fromJson(json["dob"]),
//         contact: Contact.fromJson(json["contact"]),
//         citizenship: json["citizenship"],
//         guardianName: json["guardian_name"],
//         guardianMobile: json["guardian_mobile"],
//         languages: json["languages"],
//         category: json["category"],
//         differentlyAbled: json["differently_abled"],
//         disabilityType: json["disability_type"],
//         disabilityPer: DisabilityPer.fromJson(json["disability_per"]),
//         academic: Academic.fromJson(json["academic"]),
//         studentStatus: json["student_status"],
//         studentPhoto: json["student_photo"],
//         bio: json["bio"],
//         skills: json["skills"],
//         the61B5D8B85A0D0000C0005D05: json["61b5d8b85a0d0000c0005d05"] == null ? null : json["61b5d8b85a0d0000c0005d05"],
//         curYop: json["cur_yop"] == null ? null : json["cur_yop"],
//     );

//     Map<String, dynamic> toJson() => {
//         "_id": id.toJson(),
//         "ktu_id": ktuId,
//         "first_name": firstName,
//         "second_name": secondName,
//         "email": email,
//         "gender": gender,
//         "dob": dob.toJson(),
//         "contact": contact.toJson(),
//         "citizenship": citizenship,
//         "guardian_name": guardianName,
//         "guardian_mobile": guardianMobile,
//         "languages": languages,
//         "category": category,
//         "differently_abled": differentlyAbled,
//         "disability_type": disabilityType,
//         "disability_per": disabilityPer.toJson(),
//         "academic": academic.toJson(),
//         "student_status": studentStatus,
//         "student_photo": studentPhoto,
//         "bio": bio,
//         "skills": skills,
//         "61b5d8b85a0d0000c0005d05": the61B5D8B85A0D0000C0005D05 == null ? null : the61B5D8B85A0D0000C0005D05,
//         "cur_yop": curYop == null ? null : curYop,
//     };
// }

// class Academic {
//     Academic({
//         required this.facultyAdv,
//         required this.the10Cgpa,
//         required this.the10Yop,
//         required this.the10Inst,
//         required this.the10Board,
//         required this.the12Cgpa,
//         required this.the12Yop,
//         required this.the12Inst,
//         required this.the12Board,
//         required this.ugProgram,
//         required this.ugCgpa,
//         required this.ugYop,
//         required this.ugInst,
//         required this.ugBoard,
//         required this.curProgram,
//         required this.curYop,
//         required this.jobPreference,
//         required this.internship,
//         required this.curCgpa,
//         required this.sgpa1,
//         required this.sgpa2,
//         required this.sgpa3,
//         required this.sgpa4,
//         required this.sgpa5,
//         required this.sgpa6,
//         required this.sgpa7,
//         required this.sgpa8,
//         required this.lateral,
//         required this.curBacklog,
//         required this.noBacklog,
//         required this.backlogHst,
//         required this.noBacklogHst,
//     });

//     String facultyAdv;
//     DisabilityPer the10Cgpa;
//     String the10Yop;
//     String the10Inst;
//     String the10Board;
//     DisabilityPer the12Cgpa;
//     String the12Yop;
//     String the12Inst;
//     String the12Board;
//     String ugProgram;
//     DisabilityPer ugCgpa;
//     String ugYop;
//     String ugInst;
//     String ugBoard;
//     String curProgram;
//     String curYop;
//     String jobPreference;
//     String internship;
//     DisabilityPer curCgpa;
//     DisabilityPer sgpa1;
//     DisabilityPer sgpa2;
//     DisabilityPer sgpa3;
//     DisabilityPer sgpa4;
//     DisabilityPer sgpa5;
//     DisabilityPer sgpa6;
//     DisabilityPer sgpa7;
//     DisabilityPer sgpa8;
//     String lateral;
//     String curBacklog;
//     int noBacklog;
//     String backlogHst;
//     int noBacklogHst;

//     factory Academic.fromJson(Map<String, dynamic> json) => Academic(
//         facultyAdv: json["faculty_adv"],
//         the10Cgpa: DisabilityPer.fromJson(json["10_cgpa"]),
//         the10Yop: json["10_yop"],
//         the10Inst: json["10_inst"],
//         the10Board: json["10_board"],
//         the12Cgpa: DisabilityPer.fromJson(json["12_cgpa"]),
//         the12Yop: json["12_yop"],
//         the12Inst: json["12_inst"],
//         the12Board: json["12_board"],
//         ugProgram: json["ug_program"],
//         ugCgpa: DisabilityPer.fromJson(json["ug_cgpa"]),
//         ugYop: json["ug_yop"],
//         ugInst: json["ug_inst"],
//         ugBoard: json["ug_board"],
//         curProgram: json["cur_program"],
//         curYop: json["cur_yop"],
//         jobPreference: json["job_preference"],
//         internship: json["internship"],
//         curCgpa: DisabilityPer.fromJson(json["cur_cgpa"]),
//         sgpa1: DisabilityPer.fromJson(json["sgpa1"]),
//         sgpa2: DisabilityPer.fromJson(json["sgpa2"]),
//         sgpa3: DisabilityPer.fromJson(json["sgpa3"]),
//         sgpa4: DisabilityPer.fromJson(json["sgpa4"]),
//         sgpa5: DisabilityPer.fromJson(json["sgpa5"]),
//         sgpa6: DisabilityPer.fromJson(json["sgpa6"]),
//         sgpa7: DisabilityPer.fromJson(json["sgpa7"]),
//         sgpa8: DisabilityPer.fromJson(json["sgpa8"]),
//         lateral: json["lateral"],
//         curBacklog: json["cur_backlog"],
//         noBacklog: json["no_backlog"],
//         backlogHst: json["backlog_hst"],
//         noBacklogHst: json["no_backlog_hst"],
//     );

//     Map<String, dynamic> toJson() => {
//         "faculty_adv": facultyAdv,
//         "10_cgpa": the10Cgpa.toJson(),
//         "10_yop": the10Yop,
//         "10_inst": the10Inst,
//         "10_board": the10Board,
//         "12_cgpa": the12Cgpa.toJson(),
//         "12_yop": the12Yop,
//         "12_inst": the12Inst,
//         "12_board": the12Board,
//         "ug_program": ugProgram,
//         "ug_cgpa": ugCgpa.toJson(),
//         "ug_yop": ugYop,
//         "ug_inst": ugInst,
//         "ug_board": ugBoard,
//         "cur_program": curProgram,
//         "cur_yop": curYop,
//         "job_preference": jobPreference,
//         "internship": internship,
//         "cur_cgpa": curCgpa.toJson(),
//         "sgpa1": sgpa1.toJson(),
//         "sgpa2": sgpa2.toJson(),
//         "sgpa3": sgpa3.toJson(),
//         "sgpa4": sgpa4.toJson(),
//         "sgpa5": sgpa5.toJson(),
//         "sgpa6": sgpa6.toJson(),
//         "sgpa7": sgpa7.toJson(),
//         "sgpa8": sgpa8.toJson(),
//         "lateral": lateral,
//         "cur_backlog": curBacklog,
//         "no_backlog": noBacklog,
//         "backlog_hst": backlogHst,
//         "no_backlog_hst": noBacklogHst,
//     };
// }

// class DisabilityPer {
//     DisabilityPer({
//         required this.numberDecimal,
//     });

//     String numberDecimal;

//     factory DisabilityPer.fromJson(Map<String, dynamic> json) => DisabilityPer(
//         numberDecimal: json["\u0024numberDecimal"],
//     );

//     Map<String, dynamic> toJson() => {
//         "\u0024numberDecimal": numberDecimal,
//     };
// }

// class Contact {
//     Contact({
//         required this.mobile,
//         required this.curAddress,
//         required this.perAddress,
//         required this.pincode,
//         required this.linkedIn,
//         required this.state,
//     });

//     String mobile;
//     String curAddress;
//     String perAddress;
//     String pincode;
//     String linkedIn;
//     String state;

//     factory Contact.fromJson(Map<String, dynamic> json) => Contact(
//         mobile: json["mobile"],
//         curAddress: json["cur_address"],
//         perAddress: json["per_address"],
//         pincode: json["pincode"],
//         linkedIn: json["linked_in"],
//         state: json["state"],
//     );

//     Map<String, dynamic> toJson() => {
//         "mobile": mobile,
//         "cur_address": curAddress,
//         "per_address": perAddress,
//         "pincode": pincode,
//         "linked_in": linkedIn,
//         "state": state,
//     };
// }

// class Dob {
//     Dob({
//         required this.date,
//     });

//     DateTime date;

//     factory Dob.fromJson(Map<String, dynamic> json) => Dob(
//         date: DateTime.parse(json["\u0024date"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "\u0024date": date.toIso8601String(),
//     };
// }

// class Id {
//     Id({
//         required this.oid,
//     });

//     String oid;

//     factory Id.fromJson(Map<String, dynamic> json) => Id(
//         oid: json["\u0024oid"],
//     );

//     Map<String, dynamic> toJson() => {
//         "\u0024oid": oid,
//     };
// }

// To parse this JSON data, do
//
//     final studentDbModel = studentDbModelFromJson(jsonString);

// ignore_for_file: file_names

import 'dart:convert';

import 'package:mongo_dart/mongo_dart.dart';

StudentDbModel studentDbModelFromJson(String str) =>
    StudentDbModel.fromJson(json.decode(str));

String studentDbModelToJson(StudentDbModel data) => json.encode(data.toJson());

class StudentDbModel {
  StudentDbModel({
    required this.id,
    required this.ktuId,
    required this.firstName,
    required this.secondName,
    required this.email,
    required this.gender,
  });

  ObjectId id;
  String ktuId;
  String firstName;
  String secondName;
  String email;
  String gender;

  factory StudentDbModel.fromJson(Map<String, dynamic> json) => StudentDbModel(
        id: json["_id"],
        ktuId: json["ktu_id"],
        firstName: json["first_name"],
        secondName: json["second_name"],
        email: json["email"],
        gender: json["gender"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "ktu_id": ktuId,
        "first_name": firstName,
        "second_name": secondName,
        "email": email,
        "gender": gender,
      };
}
