// ignore: unnecessary_import
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class campaign extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Campaign-2022'),
      ),

      body: Center(
          child: Column(
              children: [
                Text(
                  "2022 Placement drive\n",
                  style: TextStyle(fontSize: 30),
                ),
                Text(
                  "RIT 2022 Placement drive\n",
                  style: TextStyle(fontSize: 24),
                ),
                TextButton(

                  child: Text('Register'),
                  style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue,
                      onSurface: Colors.grey,
                      textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 20,

                      )
                  ),

                  onPressed: () {
                    //print('Pressed');
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => _buildPopupDialog(context),
                    );
                  },
                ),
              ]
          ),

      ),
    );
  }
  Widget _buildPopupDialog(BuildContext context) {
    return new AlertDialog(
      //title: const Text('Popup example'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Registered Successfully"),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }
}