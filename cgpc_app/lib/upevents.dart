import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class upevents extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Upcoming Events'),
      ),

      body: Center(
        child: Column(
            children: [
              Text(
                "This Month Events\n",
                style: TextStyle(
                    height: 1.2,
                    fontSize: 32),

              ),

              Text(
                "2022-02-18\nCGPC Training\nCgpc training 3.00-5.30",
                //textAlign: TextAlign.left,
                //overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    //height: 1.2,
                    fontSize: 22),
                //maxLines: 2,
              ),
              Text(
                "\n2022-02-21\nTCS NQT Exam",
                //textAlign: TextAlign.left,
                //overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    //height: 1.2,
                    fontSize: 22),
                //maxLines: 2,
              ),

            ]
        ),

      ),
    );
  }
}