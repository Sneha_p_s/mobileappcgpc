import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class jobs extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Eligible Jobs'),
      ),

      body: Center(
        child: Column(
            children: [
              Text(
                "Cognizant\n",
                style: TextStyle(
                  height: 1.2,
                    fontSize: 32),

              ),

              Text(
                "\nDesignation : Programmer Analyst",
                //textAlign: TextAlign.left,
                //overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    //height: 1.2,
                    fontSize: 22),
                //maxLines: 2,
              ),
              Text(
                "\nCost to Company : 6.75",
                //textAlign: TextAlign.left,
                //overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 22),
                //maxLines: 2,
              ),
              Text(
                "\nApplication Deadline : 16-02-2022 04:30:00 pm",
                //textAlign: TextAlign.left,
                //overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 22),
                //maxLines: 2,
              ),
              Text(
                "\nLocation : Banglore",
                //textAlign: TextAlign.left,
                //overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 22),
                //maxLines: 2,
              ),
              Text(
                "\nEligibility Criteria : Required CGPA 7",
                //textAlign: TextAlign.left,
                //overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 22),
                //maxLines: 2,
              ),
              TextButton(

                child: Text('Apply'),
                style: TextButton.styleFrom(
                  primary: Colors.white,
                  backgroundColor: Colors.blue,
                  onSurface: Colors.grey,
                  textStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 20,

                  )
                ),

                onPressed: () {
                  //print('Pressed');
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => _buildPopupDialog(context),
                  );
                },
              ),
            ]
        ),

      ),
    );
  }
  Widget _buildPopupDialog(BuildContext context) {
    return new AlertDialog(
      //title: const Text('Popup example'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Applied"),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }
}