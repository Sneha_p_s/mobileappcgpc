// ignore_for_file: file_names, unnecessary_new, prefer_const_constructors, duplicate_ignore, camel_case_types, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
//import 'package:faker/faker.dart';
import 'package:mongo_dart/mongo_dart.dart' as M;
import 'package:cgpc_app/studentDBModel.dart';
import 'package:cgpc_app/dbHelpper/mongodb.dart';

class studentDbInsert extends StatefulWidget {
  studentDbInsert({Key? key}) : super(key: key);

  @override
  _MongoDbInsertState createState() => _MongoDbInsertState();
}

class _MongoDbInsertState extends State<studentDbInsert> {
  var ktuidController = new TextEditingController();
  var fnameController = new TextEditingController();
  var lnameController = new TextEditingController();
  var emaiController = new TextEditingController();
  var genderController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_literals_to_create_immutables
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            // ignore: prefer_const_constructors
            Text(
              "Student Details",
              style: TextStyle(fontSize: 22),
            ),
            SizedBox(
              height: 50,
            ),
            TextField(
              controller: ktuidController,
              decoration: InputDecoration(labelText: "KTU ID"),
            ),
            TextField(
              controller: fnameController,
              decoration: InputDecoration(labelText: "First Name"),
            ),
            TextField(
              controller: lnameController,
              decoration: InputDecoration(labelText: "Second Name"),
            ),
            TextField(
              controller: emaiController,
              decoration: InputDecoration(labelText: "Email Id"),
            ),
            TextField(
              controller: genderController,
              decoration: InputDecoration(labelText: "Gender"),
            ),
            // TextField(
            //   decoration: InputDecoration(labelText: "Address"),
            // ),
            // TextField(
            //   controller: addressController,
            //   minLines: 3,
            //   maxLines: 5,
            //   decoration: InputDecoration(labelText: "Address "),
            // ),
            SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                OutlinedButton(
                    onPressed: () {
                      //  _fakeData();
                    },
                    child: Text("Generate Data")),
                ElevatedButton(
                    onPressed: () {
                      _insertStudentData(
                          ktuidController.text,
                          fnameController.text,
                          lnameController.text,
                          emaiController.text,
                          genderController.text);
                    },
                    child: Text("Insert Data"))
              ],
            )
          ],
        ),
      )),
    );
  }

  Future<void> _insertStudentData(String ktuid, String fName, String lName,
      String email, String gender) async {
    var _id = M.ObjectId();
    final data = StudentDbModel(
        id: _id,
        ktuId: ktuid,
        firstName: fName,
        secondName: lName,
        email: email,
        gender: gender);
    var result = await MongoDatabase.insert(data);
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text("DATA Inserted ")));
    _clearAll();
  }

//.showSnackBar(SnackBar(content: Text("DATA Inserted " + _id.$oid)));
  void _clearAll() {
    ktuidController.text = "";
    fnameController.text = "";
    lnameController.text = "";
    emaiController.text = "";
    genderController.text = "";
  }

  // void _fakeData() {
  //   setState(() {
  //     // ignore: prefer_typing_uninitialized_variables
  //     fnameController.text = faker.person.firstName();
  //     lnameController.text = faker.person.lastName();
  //     addressController.text =
  //         faker.address.streetName() + "\n" + faker.address.streetAddress();
  //   });
  // }
}
