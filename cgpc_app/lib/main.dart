// import 'package:cgpc_app/Screens/Welcome/welcome_screen.dart';
// import 'package:cgpc_app/studentRegistration.dart';
// import 'package:flutter/material.dart';
// import 'package:cgpc_app/dbHelpper/mongodb.dart';

// void main() async {
//   WidgetsFlutterBinding.ensureInitialized();
//   await MongoDatabase.connect();
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'CGPC APP',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         scaffoldBackgroundColor: Colors.white,
//       ),
//       home: WelcomeScreen(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key? key}) : super(key: key);

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   @override
//   Widget build(BuildContext context) {
//     // ignore: prefer_const_constructors
//     return Scaffold(body: SafeArea(child: Text("Hello")));
//   }
// }

import 'package:nwflut/dbHelper/campaign.dart';
import "package:flutter/material.dart";

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.blue), home: campaign());
  }
}
